<?php

use PHPUnit\Framework\TestCase;

class MyBigNumberTest extends TestCase
{
    public function testSum()
    {
        $test = new App\MyBigNumber;
        $result = $test->sum("1234", "987");
        $this->assertEquals("2221", $result);
    }

    public function testSum1()
    {
        $test = new App\MyBigNumber;
        $result = $test->sum("12345", "987");
        $this->assertEquals("2221", $result);
    }
}
