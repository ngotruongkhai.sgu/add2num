<?php

namespace App;

class MyBigNumber
{
    public function __construct()
    {
    }

    function sum($num1, $num2)
    {
        $rm = 0;
        $result = '';
        while ((int)$num1 != 0 || (int)$num2 != 0 || $rm != 0) {
            $sum = ($num1 % 10) + ($num2 % 10) + $rm;
            if ($sum >= 10) {
                $rm = 1;
                $result .= (string)($sum % 10);
            } else {
                $result .= (string)$sum;
                $rm = 0;
            }
            echo $sum . '  ';
            $num1 /= 10;
            $num2 /= 10;
        }
        return strrev($result);
    }
}
